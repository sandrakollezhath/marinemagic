/*Script for card slider */
const slider = document.querySelector('.slider');
    let slideIndex = 0;

    function showSlides() {
        slider.style.transform = `translateX(-${slideIndex * 260}px)`; // Adjust 320 based on card width
    }

    function nextSlide() {
        if (slideIndex < slider.children.length - 1) {
            slideIndex++;
            showSlides();
        }
    }

    function prevSlide() {
        if (slideIndex > 0) {
            slideIndex--;
            showSlides();
        }
    }